/**
 * Cow.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.model;

import java.math.BigDecimal;

/**
 * Clase Cow
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class Cow extends Animal {

    public Cow(Float weight) {
        super(Cow.class.getSimpleName(), weight, "Muuuuu");
    }

    @Override
    public Integer getLegs() {
        return 4;
    }

    @Override
    protected void sayGoodNight() {
        System.out.println("Cow say: Bye bye");
    }

    @Override
    protected void goToBed() {
        System.out.println("Cow sleep in the stable");
    }
}