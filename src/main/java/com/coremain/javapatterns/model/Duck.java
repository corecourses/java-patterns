/**
 * Cow.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.model;

import java.math.BigDecimal;

/**
 * Clase Cow
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class Duck extends Animal {

    public Duck(Float weight) {
        super(Duck.class.getSimpleName(), weight, "Cuacuacua");
    }

    @Override
    public Integer getLegs() {
        return 2;
    }

    @Override
    protected void sayGoodNight() {
        System.out.println("Duck say: I don't want to say goodbye");
    }

    @Override
    protected void goToBed() {
        System.out.println("Duck sleep in the pool");
    }
}