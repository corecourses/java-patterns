package com.coremain.javapatterns.model;

/**
 * Clase GreetingType
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public enum GreetingType {
    MUUU ("Muuu"),
    GRRR ("Grrrr"),
    CUA ("Cua cua"),
    UUU ("Uuu uuu");

    private final String name;

    private GreetingType(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
