/**
 * Cow.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.model;

/**
 * Clase Monkey
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class Monkey {

    private Long id;
    private String name;
    private String weight;
    private GreetingType greeting;
    private String lastAction;

    public Monkey(String weight) {
        this.id = System.currentTimeMillis();
        this.name = Monkey.class.getSimpleName();
        this.weight = weight;
        this.greeting = GreetingType.UUU;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWeight() {
        return weight;
    }

    public GreetingType getGreeting() {
        return greeting;
    }

    public String getLastAction() {
        return lastAction;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Monkey{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", weight='").append(weight).append('\'');
        sb.append(", greeting=").append(greeting);
        sb.append(", lastAction='").append(lastAction).append('\'');
        sb.append('}');
        return sb.toString();
    }
}