/**
 * Cow.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.model;

import java.math.BigDecimal;

/**
 * Clase Lion
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class Lion extends Animal {

    public Lion(Float weight) {
        super(Lion.class.getSimpleName(), weight, "Grrrrrrrr");
    }


    @Override
    public Integer getLegs() {
        return 4;
    }

    @Override
    protected void sayGoodNight() {
        System.out.println("Lion say: Goodbye");
    }

    @Override
    protected void goToBed() {
        System.out.println("Lion sleep in the jungle");
    }
}