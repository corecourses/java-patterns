package com.coremain.javapatterns.model;


import java.util.Observable;
import java.util.UUID;

public abstract class Animal extends Observable {

    protected String id;
    protected String name;
    protected Float weight;
    protected String greeting;
    protected String lastAction;

    public Animal(){
        //Default constructor
    }

    public Animal(String name, Float weight, String greeting) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.weight = weight;
        this.greeting = greeting;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Float getWeight() {
        return weight;
    }

    public String getGreeting() {
        return greeting + ". I'm a " + name;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void doAction(String action) {
        lastAction = action;
        setChanged();
        notifyObservers(action);
    }

    public void goToSleep() {
        sayGoodNight();
        goToBed();
    }

    public abstract Integer getLegs();
    protected abstract void sayGoodNight();
    protected abstract void goToBed();

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Animal{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", weight=").append(weight);
        sb.append(", greeting='").append(getGreeting()).append('\'');
        sb.append(", lastAction='").append(lastAction).append('\'');
        sb.append('}');
        return sb.toString();
    }
}