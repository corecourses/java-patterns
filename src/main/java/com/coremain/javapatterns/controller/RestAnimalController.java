/**
 * RestAnimalController.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.controller;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.patterns.behavioral.ZooObserver;
import com.coremain.javapatterns.patterns.creational.ZooSingleton;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * Clase RestAnimalController
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
@RestController
@CrossOrigin
@RequestMapping("api/v1/animals")
public class RestAnimalController {

    @GetMapping("/greeting")
    public Long greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return 1L;
    }

    @PostMapping("/create/{name}")
    public ResponseEntity<Animal> create(@PathVariable String name) {
        final Animal animal = ZooSingleton.getInstance().getZooFacade().createAnimal(name);
        return ResponseEntity.status(HttpStatus.CREATED).body(animal);
    }

    @GetMapping("/all")
    public ResponseEntity<Set<Animal>> getAllAnimals() {
        final Set<Animal> animalSet = ZooSingleton.getInstance().getAnimalSet();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(animalSet);
    }

    @DeleteMapping("/clear")
    public ResponseEntity<Object> clear() {
        ZooSingleton.getInstance().getZooFacade().clearAnimals();
        return ResponseEntity.ok().body("Removed animals of the zoo");
    }

    @PostMapping("/addzooobserver/{filename}")
    public ResponseEntity<Object> addZooObserver(@PathVariable String filename) {
        String fullFilename = filename + ".txt";
        ZooSingleton.getInstance().addObserver(new ZooObserver(fullFilename));
        return ResponseEntity.ok().body("ZooObserver " + fullFilename + " added to zoo");
    }

    @DeleteMapping("/removeallzooobserver")
    public ResponseEntity<Object> removeAllZooObserver() {
        ZooSingleton.getInstance().deleteObservers();
        return ResponseEntity.ok().body("Remove all zoo observers");
    }

    @PostMapping("/doanimalaction/{id}/{action}")
    public ResponseEntity<Object> doAnimalAction(@PathVariable String id, @PathVariable String action) {
        final Animal animal = ZooSingleton.getInstance().getZooFacade().animalDoAction(id, action);
        return ResponseEntity.ok().body(animal);
    }

    @PostMapping("/closezoo")
    public ResponseEntity<Object> closeZoo() {
        ZooSingleton.getInstance().getZooFacade().closeZoo();
        return ResponseEntity.ok().body("Zoo closed");
    }
}