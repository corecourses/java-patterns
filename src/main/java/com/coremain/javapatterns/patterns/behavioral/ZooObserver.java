/**
 * Observer.java 08/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.patterns.behavioral;

import com.coremain.javapatterns.model.Animal;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Observable;
import java.util.Observer;

/**
 * Clase ZooObserver. Este observador se encarga observar las acciones de los animales del zoo
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class ZooObserver implements Observer {
    final private String fileName;

    public ZooObserver(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public void update(Observable o, Object animal) {
        try {
            String fullPathName = "c:/zoo/" + fileName;
            try {
                Files.createFile(Paths.get(fullPathName));
            } catch (FileAlreadyExistsException ignored) {
            }
            String rowFile = ((Animal) animal).toString()+"\r\n";
            Files.write(Paths.get(fullPathName), rowFile.getBytes(),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}