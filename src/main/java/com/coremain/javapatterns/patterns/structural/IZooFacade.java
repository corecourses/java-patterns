package com.coremain.javapatterns.patterns.structural;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.patterns.creational.ZooSingleton;

/**
 * Clase IZooFacade
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public interface IZooFacade {
    Animal createAnimal(String animalName);

    Animal animalDoAction(String id, String action);

    void clearAnimals();

    void closeZoo();
}
