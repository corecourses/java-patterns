/**
 * MonkeyAdapter.java 08/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.patterns.structural;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.model.Monkey;

/**
 * Clase MonkeyAdapter
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class MonkeyAdapter extends Animal {

    private final Monkey monkey;

    public MonkeyAdapter(Monkey monkey) {
        this.monkey = monkey;
    }

    @Override
    public Float getWeight() {
        return Float.valueOf(monkey.getWeight());
    }

    @Override
    public String getGreeting() {
        return monkey.getGreeting().toString()+". I'm a "+getName();
    }

    @Override
    public String getId() {
        return monkey.getId().toString();
    }

    @Override
    public String getName() {
        return monkey.getName();
    }

    @Override
    public Integer getLegs() {
        return 2;
    }

    @Override
    protected void sayGoodNight() {
        System.out.println("Monkey say: I'm an adapter. Bye!");
    }

    @Override
    protected void goToBed() {
        System.out.println("Monkey sleep in the tree.");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Animal{");
        sb.append("id='").append(getId()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", weight=").append(getWeight());
        sb.append(", greeting='").append(getGreeting()).append('\'');
        sb.append(", lastAction='").append(getLastAction()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}