/**
 * ZooFacade.java 09/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.patterns.structural;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.patterns.creational.AnimalFactory;
import com.coremain.javapatterns.patterns.creational.ZooSingleton;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Clase ZooFacade
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class ZooFacade implements IZooFacade{
    private AnimalFactory animalFactory;

    public Animal createAnimal(String animalName) {
        final Animal animal = ZooSingleton.getInstance().getAnimalFactory().getAnimal(animalName);
        ZooSingleton.getInstance().getAnimalSet().add(animal);
        return animal;
    }

    public Animal animalDoAction(String id, String action) {
        Animal animal = getAnimal(id);
        animal.doAction(action);
        ZooSingleton.getInstance().notifySubscribers(animal);
        return animal;
    }

    public void clearAnimals() {
        ZooSingleton.getInstance().getAnimalSet().clear();
    }

    @Override
    public void closeZoo() {
        ZooSingleton.getInstance().getAnimalSet().forEach(Animal::goToSleep);
    }


    /**
     * Get animal by id
     *
     * @param id
     *         animal id
     * @return Animal
     * @throws NoSuchElementException
     */
    private Animal getAnimal(String id) throws NoSuchElementException {
        final Set<Animal> animalSet = ZooSingleton.getInstance().getAnimalSet();
        final List<Animal> animalList =
                animalSet.stream().filter(animal -> animal.getId().equals(id)).collect(Collectors.toList());
        if (animalList.size() != 1) {
            throw new NoSuchElementException("Animal " + id + " not found!");
        }

        return animalList.get(0);
    }

}