/**
 * Zoo.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.patterns.creational;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.patterns.structural.IZooFacade;
import com.coremain.javapatterns.patterns.structural.ZooFacade;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Clase ZooSingleton
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class ZooSingleton extends Observable {

    private static ZooSingleton instance;
    private AnimalFactory animalFactory;
    private IZooFacade zooFacade;
    private Set<Animal> animalSet;

    // Constructor privado
    private ZooSingleton() {
        animalFactory = new AnimalFactory();
        animalSet = new HashSet<>();
        zooFacade = new ZooFacade();
    }

    // Creador sincronizado para protegerse de posibles problemas multi-hilo
    private static synchronized void createInstance() {
        if (instance == null) {
            instance = new ZooSingleton();
        }
    }

    public static ZooSingleton getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public AnimalFactory getAnimalFactory() {
        return animalFactory;
    }

    public IZooFacade getZooFacade() {
        return zooFacade;
    }

    public void notifySubscribers(Animal animal){
        setChanged();
        notifyObservers(animal);
    }

    public Set<Animal> getAnimalSet() {
        return animalSet;
    }
}