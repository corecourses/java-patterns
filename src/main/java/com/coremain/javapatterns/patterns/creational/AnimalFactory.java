/**
 * AnimalFactory.java 07/10/2020
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.coremain.javapatterns.patterns.creational;

import com.coremain.javapatterns.model.Animal;
import com.coremain.javapatterns.model.Cow;
import com.coremain.javapatterns.model.Duck;
import com.coremain.javapatterns.model.Lion;
import com.coremain.javapatterns.model.Monkey;
import com.coremain.javapatterns.patterns.structural.MonkeyAdapter;

import java.util.NoSuchElementException;

/**
 * Clase AnimalFactory
 *
 * @author <a href="mailto:emiliop@ext.inditex.com">Emilio Paleo Barreira</a>
 */
public class AnimalFactory {

    public Animal getAnimal(String animalType) {
        if (animalType == null) {
            return null;
        }
        switch (animalType) {
            case "cow":
                return new Cow(100f);
            case "lion":
                return new Lion(200f);
            case "duck":
                return new Duck(10f);
            case "monkey":
                return new MonkeyAdapter(new Monkey("80"));
            default:
                throw new NoSuchElementException("Animal not found");
        }
    }
}