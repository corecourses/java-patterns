## [java-patterns](https://gitlab.com/poketilio/java-patterns.git)


## Introducción

Este proyecto recoge los patrones de diseño más comunes
para explicarlos con ejemplos.

## Índice

- [Enlaces](#enlaces)
- [Comandos](#comandos)
- [Patrones Creacionales](#patrones-creacionales)
    - [Abstract Factory](#abstract-factory)
    - [Singleton](#singleton)
    - [Builder](#builder)
- [Patrones de Comportamiento](#patrones-de-comportamiento)
    - [Strategy](#strategy)
    - [Observer](#observer)
    - [Template Method](#template-method)
- [Patrones Estructurales](#patrones-estructurales)
    - [Adapter](#adapter)
    - [Decorator](#decorator)
    - [Facade](#facade)

## Enlaces

- [refactoring.guru](https://refactoring.guru)
- [mcdonaldland.info](http://www.mcdonaldland.info/files/designpatterns/designpatternscard.pdf)
- [java-design-patterns.com](http://java-design-patterns.com/patterns/)
- [wikipedia](https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o)
- [github/alxgcrz](https://github.com/alxgcrz/design-patterns-java)
- [github/iluwatar](https://github.com/iluwatar/java-design-patterns)
- [github/fengjundev](https://github.com/fengjundev/Java-Design-Patterns)

## Libros

- [Head First Design Patterns (A Brain Friendly Guide)](https://www.amazon.es/First-Design-Patterns-Brain-Friendly/dp/0596007124/ref=asc_df_0596007124/?tag=googshopes-21&linkCode=df0&hvadid=54582498915&hvpos=1o1&hvnetw=g&hvrand=7692923997913972546&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1005482&hvtargid=pla-138214723035&psc=1)
- The Gang of Four


## Quickstart

Descargamos el repositorio GIT que necesitamos instalar a nuestro repo local, en el siguiente enlace para clonar el repo:
https://gitlab.com/corecourses/java-patterns.git

Importar el proyecto y ejecutar con Spring-Boot: **spring-boot:run**

## Patrones Creacionales


### [Abstract Factory](src/main/java/com/coremain/javapatterns/patterns/creational/AnimalFactory.java)

Crea objetos de diferentes familias de clases. Es un Factory para crear Factories.

En el ejemplo se necesita crear objetos gráficos como: Button, Panel, Windows, TextField, entre otros.
Debido a que se desea que la aplicación sea multiplataforma, se crean Factories para crear los objetos
gráficos dependiendo de la plataforma (Windows, Linux).

![abstractfactory](plantuml/creational/abstractfactory.png)

Ejemplo de uso:

```
GuiFactory guiFactory = GuiFactorySelector.getFactory(OS.LINUX);

Button button = guiFactory.createButton(); // Botón que funcionará solo en linux
button.paint(); // Salida: Linux Button

Panel panel = guiFactory.createPanel(); // Panel que funcionará solo en linux
panel.paint(); // Salida: Linux Panel
```

Este patrón puede ser usado cuando una solución debe funcionar con diferentes variantes de una familia de objetos.
Abstrae al desarrollador de la creación de los objetos, y solo tiene la responsabilidad de crear objetos asociados entre sí.

### [Singleton](src/main/java/com/coremain/javapatterns/patterns/creational/ZooSingleton.java)

Singleton es un patrón creacional que hace que exista solo una instancia para un tipo de dato.
Permite el acceso global a la instancia. Es usado cuando se necesita que compartir un recurso en todo la aplicación,
como por ejemplo la conexión a la base de datos. 

En el ejemplo se presenta un uso común, el acceso a configuraciones en la aplicación.
Generalmente las configuraciones son compartidas, y se necesita acceder a ellas desde cualquier
punto de la aplicación, además es necesario que estas se actualizen para toda la aplicación por igual.

![singleton](plantuml/creational/singleton.png)

Ejemplo de uso:

```
Settings settings = Settings.getInstance();

settings.set("db.name", "test");

System.out.println(Settings.getInstance().get("db.name")); // Se llama al método getInstance de nuevo. Salida: test
```

### [Builder](src/main/java/com/coremain/javapatterns/patterns/creational/builder)

Permite producir diferentes tipos y representaciones de un objeto utilizando el mismo proceso de construcción. 
El builder permite construir objetos complejos paso a paso.
Ayuda a que el código sea más legible. Y evita usar constructores, los 
cuales afectan a muchas clases cuando se cambia su firma.
También encapsula lógica especifica de construcción del objeto,
abstrayendo al usuario de este conocimiento.

![builder](plantuml/creational/builder.png)

Ejemplo de uso:

```
Person person = Person.builder()
                .firstName("Nikola")
                .lastName("Tesla")
                .build();

System.out.println(person);
```


## Patrones de Comportamiento


### [Strategy](src/main/java/pattern/behavioral/strategy)

Permite definir una familia de algoritmos, encapsular cada uno y hacerlos intercambiables. El patrón permite que el algoritmo varíe independientemente de los clientes que lo utilizan.

Es útil cuando se tienes un objeto que debería poder hacer la misma tarea de muchas maneras diferentes. 
Esas tareas se pueden descomponer en clases de una misma familia.

![strategy](plantuml/behavioral/strategy.png)

Ejemplo de uso:

```
Compressor compressor = new Compressor();
compressor.setCompressionFormat(new ZipCompression());
	
compressor.compress(asList(new File("README.md"))); // Salida: Compressing [README.md] with zip format
```


### [Observer](src/main/java/com/coremain/javapatterns/patterns/behavioral/ZooObserver.java)

Observer es un patrón de diseño de comportamiento que permite definir
un mecanismo de suscripción para notificar a varios objetos
sobre cualquier evento que ocurra al objeto que están observando.

Es un patrón muy utilizado en las interfaces gráficas.

En el ejemplo cuando se hace clic en un botón se notifica a los observadores
'guardar archivo' y 'mostrar mensaje'.

![observer](plantuml/behavioral/observer.png)

Ejemplo de uso:

```
Button button = new Button();
button.addListener(new SaveFileListener());
button.addListener(new ShowMessageListener());
button.click(); // Se notificará a los observers
```


### [Template Method](src/main/java/com/coremain/javapatterns/model/Animal.java)

El Método de plantilla es un patrón de diseño de 
comportamiento que define el esqueleto de un algoritmo en la superclase, 
pero permite que las subclases sobrescriban los pasos específicos 
del algoritmo sin cambiar su estructura.

En el ejemplo un objeto animatable sobrescribe solamente 'animate' y 'paint'.
El objeto cliente (Game) no llama directamente los métodos plantillas (pasos específicos),
sino que invoca 'updateFrame' (el Template Method).

![templatemethod](plantuml/behavioral/templatemethod.png)

Ejemplo de uso:

```java
/**
 * Un objeto 2D.
 */
public abstract class Animatable {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * Este es el "Template Method".
     * Puede ser final para que no se herede.
     */
    public void updateFrame() {
        animate();
        paint();
    }

    public abstract void animate();

    public abstract void paint();
}
```


## Patrones Estructurales

### [Adapter](src/main/java/com/coremain/javapatterns/patterns/structural/MonkeyAdapter.java)

Un adaptador envuelve (wraps) la lógica de un objeto, la cual no puede ser
alcanzada debido a interfaces incompatibles.

Es muy util cuando se quiere incluir una clase de terceros
a nuestra base de código pero, esta usa o retorna tipos de objetos no
compatibles con nuestra aplicación. Es necesario que el adaptador pertenezca (herede)
de una familia de clases.

![adapter](plantuml/structural/adapter.png)

Ejemplo de uso:

```
Fahrenheit fahrenheit = new Fahrenheit(98.6);

List<MetricTemperatureSystem> temperatureList = Arrays.asList(
    new Celsius(37),
    new CelsiusAdapter(fahrenheit)
);

System.out.println(fahrenheit); // Salida: 98.6° F
System.out.println(temperatureList); // Salida: [37.0° C, 37.0° C]
```


### [Decorator](src/main/java/com/coremain/javapatterns/patterns/structural)

Decorator es un patrón que permite agregar nuevos comportamientos 
a los objetos al colocarlos dentro de otros objetos envoltorios (wrappers) especiales.

El patrón [adapter](#adapter) cambia la interfaz de un objeto existente, 
mientras que el decorator mejora un objeto sin cambiar su interfaz. 
Además, admite la composición recursiva, 
que no es posible cuando se usa el adapter.

![decorator](plantuml/structural/decorator.png)

Ejemplo de uso:

```
ConsoleDataSource consoleDataSource = new ConsoleDataSource();
String data = consoleDataSource.readData();
consoleDataSource.writeData(data);

EncryptionDecorator encryptionDecorator = new EncryptionDecorator(consoleDataSource);
encryptionDecorator.writeData(data);
```

### [Facade](src/main/java/com/coremain/javapatterns/patterns/structural/ZooFacade.java)

Facade es un patrón de diseño estructural que proporciona una interfaz 
simplificada para una biblioteca, un framework o cualquier otro 
conjunto complejo de clases (subsistema). 

Permite separar y reemplazar de manera sencilla diferentes subsistemas. 
Disminuye el acoplamiento y dependencia de un conjunto de clases hacia otro.

En el ejemplo el cliente no necesita saber nada sobre la implementación del subsistema,
excepto la inicialización de la clase controladora para la base de datos requerida.
El cliente simplemente interactúa con las interfaces de la fachada en lugar de clases
específicas de la base de datos.
DriverManager actúa como una fachada para el subsistema subyacente.

Entre los ejemplos más conocidos tenemos JDBC y SLF4J.

![facade](plantuml/structural/facade.png)

Ejemplo de uso:

```
Class.forName("pattern.structural.facade.oracle.OracleJDBCDriver");
Connection connection = DriverManager.getConnection("oracle");
Statement statement = connection.createStatement();
statement.executeQuery("select * from employee");

Class.forName("pattern.structural.facade.mysql.MySQLJDBCDriver");
connection = DriverManager.getConnection("mysql");
statement = connection.createStatement();
statement.executeQuery("select * from employee");
```